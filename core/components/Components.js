const fs = require('fs');
const Hyperd = require("hyperd");
const ComponentsList = require("./ComponentsList");
const Handlebars = require("handlebars");
const observable = require('observable');
const EventEmitter = require('events');
const Ps = require('perfect-scrollbar');
// const Qwery = require('qwery');


var selector,
    template;

class Components {

    constructor(component, params, callback) {
        var loadChildComponents = observable();
        var self = this;

        this.viewInstance = component;
        this.selector = component.selector;
        this.template = component.template;
        this.router = component.router;

        this.component = new component.view();
        this.component["params"] = params;
        this.component.widgets = [];

        this.component.perfectScrollbar;

        this.node;

        this.components = {};

        this.getSelector().then((selector) => {
            this.getTemplate().then((template) => {
                this.render(selector, template, () => {
                    if (typeof component.parent != "undefined") {
                        this.component.parent = component.parent;
                    }
                    if (typeof this.component.onInit != "undefined") {
                        this.component.onInit(this.node);
                    }
                    callback(this.component);
                });
            });
        });
    }

    /**
     * [setTemplate description]
     * @param {[type]} location [description]
     */
    static setTemplate(location) {
        this.templateLocation = location;
    }

    run(id) {

    }

    /**
     * [setTemplate description]
     * @param {[type]} location [description]
     */
    getTemplate() {

        return new Promise((resolve, reject) => {

            fs.readFile(this.template + '.html', 'utf8', (err, template) => {
                if (err) {
                    reject(err);
                    return false;
                }

                var templateHTML = template;
                resolve(templateHTML);
            });
        });
    }

    /**
     * [setSelector description]
     * @param {[type]} selector [description]
     */
    static setSelector(selector) {
        this.selector = selector;
    }

    /**
     * [setTemplate description]
     * @param {[type]} location [description]
     */
    getSelector() {
        return new Promise((resolve, reject) => {
            var tag = document.getElementsByTagName(this.selector)[0];
            if (typeof ComponentsList[this.selector] != "undefined") {
                var component = ComponentsList[this.selector];
                // component.destroy();
                tag.removeChild(tag.childNodes[0]);
            }

            var div = document.createElement("div");
            div.id = this.selector;
            tag.appendChild(div);
            // this.component.perfectScrollbar
            var el = document.getElementById(this.selector);
            console.log(el);
            this.component.perfectScrollbar = Ps;
            // this.component.perfectScrollbar.initialize(div);
            this.node = el;

            if (el.length != 0) {
                var selectorDOM = el;
                resolve(selectorDOM);
            } else {
                var selectorDOM = false;
                reject(selectorDOM);
            }
        });
    }

    castToObject(instance) {
        var newObj = {};

        for (let key in instance) {
            var item = instance[key];
            newObj[key] = item;
        }

        return newObj;
    }

    /**
     * [render description]
     * @return {[type]} [description]
     */
    render(selectorDOM, templateHTML, callback) {
        var self = this;

        ComponentsList[this.selector] = new Hyperd(selectorDOM, function () {
            // Check for any events fired on VDom scope.

            var templateHandlebars = Handlebars.compile(templateHTML);
            var html = templateHandlebars(this.data);
            return '<div>' + html + '</div>';
        });
        // ComponentsList[this.selector].onRender = function () {

        // }

        ComponentsList[this.selector].data = this.component;

        // this.viewInstance['node'] = Qwery(selectorDOM)[0];
        // this.component['$'] = require("jquery")(ComponentsList[this.selector].node);

        self.getEvents(ComponentsList[this.selector]);
        //
        // ComponentsList[this.selector].on('render', () => {
        //     if (typeof component != "undefined") {
        //         this.viewInstance['node'] = ComponentsList[this.selector].node;
        //     }
        // });

        // ComponentsList[this.selector].on('render', function () {
        setTimeout(function () {
            callback();
        }, 20);


        // });
    }

    /**
     * [getEvents description]
     * @param  {[type]} component [description]
     * @return {[type]}           [description]
     */
    getEvents(component) {

        var eventTypes = [
            // Form events
            'blur',
            'change',
            'focus',
            'input',
            'invalid',
            'reset',
            'search',
            'select',
            'submit',
            // Keyboard events
            'keydown',
            'keypress',
            'keyup',
            // Mouse events
            'click',
            'dblclick',
            // 'mousedown',
            // 'mouseout',
            // 'mouseover',
            // 'mouseup',
            'wheel',
            'scroll',
        ]

        for (let key in eventTypes) {
            var eventType = eventTypes[key];
            // Listen to all listed types.
            component.on(eventType, (event) => {
                var eventAction = '(' + event.type + ')';
                var eventInput = '(model)';
                if (typeof event.target[eventAction] != "undefined") {
                    //console.log(event);
                    if (event.type == 'submit') {
                        event.preventDefault();
                    }

                    // Event
                    var actionValue = event.target[eventAction];

                    // Get method name
                    var methodName = /^(.*?)\(/;

                    // Get method arguments
                    var argumentsArray = this.getArgumentList(this, actionValue);

                    // Get method
                    var getDot = this.getDot(this, actionValue.match(methodName)[1]);

                    if (typeof getDot != "undefined") {
                        this.getDot(this, actionValue.match(methodName)[1]).apply(this.component, argumentsArray);
                    }

                }
                if (typeof event.target[eventInput] != "undefined" && event.type == "input") {
                    this.setDot(this, event.target[eventInput], event.target.value);
                }
            });
        }
    }

    /**
     * [getArgumentList description]
     * @param  {[type]} scope     [description]
     * @param  {[type]} variables [description]
     * @return {[type]}           [description]
     */
    getArgumentList(scope, variables) {

        // Get method call input
        var methodInput = /\((.*)\)/;
        var inputString = variables.match(methodInput)[1];
        var argArray = inputString.split(",");
        var argVariables = [];

        for (let key in argArray) {
            if (argArray[key].indexOf('"') >= 0 || argArray[key].indexOf("'") >= 0) {

                var removeChar1 = /\'(.*)\'/;
                var removeChar1 = argArray[key].match(removeChar1);

                if (removeChar1 != null) {
                    var removedChar = removeChar1[1];
                }

                var removeChar2 = /\"(.*)\"/;
                var removeChar2 = removedChar.match(removeChar1);

                if (removeChar2 != null) {
                    var removedChar = removeChar2[1];
                }

                argVariables.push(removedChar);
            } else if (argArray[key] == "$event") {
                argVariables.push(event);
            } else {
                argVariables.push(this.getDot(this, argArray[key]));
            }
        }

        return argVariables;
    }

    /**
     * [getDotMethod description]
     * @param  {[type]} obj          [description]
     * @param  {[type]} variables    [description]
     * @param  {[type]} nestedMethod [description]
     * @return {[type]}              [description]
     */
    getDot(parent, namespace) {
        var parts = namespace.split('.'),
            current = parent.component;

        // parent.click();
        for (let i = 0; i < parts.length; i += 1) {
            if (current[parts[i]]) {
                current = current[parts[i]];
            } else {
                if (i >= parts.length - 1)
                    return undefined;
            }
        }

        return current;
    }

    /**
     * [getDotMethod description]
     * @param  {[type]} obj          [description]
     * @param  {[type]} variables    [description]
     * @param  {[type]} nestedMethod [description]
     * @return {[type]}              [description]
     */
    setDot(parent, namespace, value) {
        var parts = namespace.split('.'),
            current = parent.component;

        // parent.click();
        for (let i = 0; i < parts.length; i += 1) {
            if (current[parts[i]] && i < parts.length - 1) {
                current = current[parts[i]];
            } else {
                current[parts[i]] = value;
            }
        }
    }

    /**
     * [parseOptions description]
     * @param  {[type]} options [description]
     * @return {[type]}         [description]
     */
    parseOptions(options) {
        for (let key in options) {
            var optionValue = options[key];
            if (typeof this[key] != "undefined") {

            }
        }
    }

}

Components.ComponentsList = {};

module.exports = Components;
