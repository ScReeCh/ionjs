const bootstrapNative = require('bootstrap.native');

'use strict';

class Bootstrap {

    constructor() {
        return bootstrapNative;
    }
}

module.exports = Bootstrap;
