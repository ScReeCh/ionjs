const path = require('path');
const Ps = require('perfect-scrollbar');
const EventEmitter = require('events');

'use strict';

class Assets {

    constructor() {
        this.scrollBarsCollection = {};
    }

    css() {
        return __dirname + '/css/styles.css'
    }

    perfectScrollbarCss() {
        return __dirname + '/perfect-scrollbar/perfect-scrollbar.min.css'
    }

    scrollbar(options) {
        var scrollBar = function (options) {
            var element = (typeof options.settings == 'undefined') ? options.element : window;
            var settings = (typeof options.settings == 'undefined') ? options.settings : {};
            Ps.initialize(options.element);
            window.addEventListener('resize', function (event) {
                if (typeof options.resize != 'undefined') {
                    options.resize(window.innerHeight, window.innerWidth);
                }
            });
        };

        return new scrollBar(options);
    }
}

module.exports = new Assets();
