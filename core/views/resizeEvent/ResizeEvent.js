const EventEmitter = require('events');

'use strict';

module.exports = {
    event: function () {
        var obsurve = observable();

        obsurve({
            height: window.innerHeight,
            width: window.innerWidth
        });

        window.addEventListener('resize', function (event) {
            obsurve({
                height: window.innerHeight,
                width: window.innerWidth
            });
        });

        return {
            then: obsurve
        }
    }
};
