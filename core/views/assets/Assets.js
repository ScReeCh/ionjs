const path = require('path');

const bootstrap = require('bootstrap.native');

'use strict';

class Assets {

    constructor() {
        this.scrollBarsCollection = {};
    }

    css() {
        return __dirname + '/css/styles.css'
    }

    perfectScrollbar() {
        return __dirname + '/perfect-scrollbar/perfect-scrollbar.min.css'
    }
}

module.exports = Assets;
