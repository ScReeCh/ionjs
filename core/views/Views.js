var Ps = require('perfect-scrollbar');

'use strict';

class Views {

    constructor() {

        this.scrollBarsCollection = {};
    }

    scrollBars(scrollers) {
        for (var key in scrollers) {
            var scroller = scrollers[key];
            var container = document.getElementById(scroller);

            this.scrollBarsCollection[scrollers] = Ps.initialize(container, {
                wheelSpeed: 2,
                wheelPropagation: true,
                minScrollbarLength: 20
            });
        }
    }
}

module.exports = Views;
