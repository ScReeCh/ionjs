'use strict';

const EventEmitter = require('events');

/**
 * Used with authentication service.
 * Facilitate the authentication of a route.
 */
module.exports = class Authentication {
    /**
     * Initiats authenticate helper method
     */
    constructor() {
        this.events = new EventEmitter();
        this.authenticate();
    }

    /**
     * Called on successfull authentication
     */
    authenticated() {
        this.events.emit('authenticated');
    }

    /**
     * Placeholder method
     */
    authenticate() {

    }
}
