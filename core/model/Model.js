'use strict';

const Nedb = require('nedb');
const EventEmitter = require('events');
const fs = require('fs');

module.exports = function () {

    var nedb = {};

    class Model {

        constructor(options) {
            this.events = new EventEmitter();
            if (typeof options != "undefined") {

                if (typeof options.location != "undefined") {
                    nedb = new Nedb({
                        filename: options.location,
                        autoload: true
                    });

                    this.location = options.location;
                    this.autoload = (typeof options.autoload === "undefined") ? true : options.autoload;
                    this.schema = (typeof options.schema === "undefined") ? {} : options.schema;

                    fs.readFile(options.location, 'utf8', (loadError, template) => {
                        nedb.loadDatabase((err) => {
                            if (err) {
                                console.error(err);
                                return false;
                            }

                            this.saveData = {};
                            this.errors = [];
                            this.events.emit('loaded');
                        })
                    });
                }
            }
        }

        find(query, callback) {
            nedb.find(query, callback);
        }

        reset() {
            fs.unlinkSync(this.location);
        }

        insert(data, callback) {
            // this.validateSchema(data);
            // if (this.errors.length < 0) {
            nedb.insert(data, (err, newData) => {
                this.events.emit('insert');
                callback(err, newData)
            });
            // } else {
            //     callback(this.errors, null)
            // }
        }

        update(data, callback) {
            this.events.emit('update');
        }

        validateSchema(data) {

            for (let key in this.schema) {
                if (typeof data[key] != "undefined") {
                    this.saveData[key] = data[key];
                    switch (this.schema[key]) {
                    case 'number':
                        this.saveData[key] = (typeof data[key] === 'number' ? data[key] : false);
                        break;
                    case 'string':
                        this.saveData[key] = (typeof data[key] === 'string' ? data[key] : false);
                        break;
                    case 'boolean':
                        this.saveData[key] = (typeof data[key] === 'boolean' ? data[key] : false);
                        break;
                    case 'email':
                        this.saveData[key] = (typeof data[key] === 'boolean' ? data[key] : false);
                        break;
                    case 'any':
                        this.saveData[key] = data[key];
                        break;
                    }
                    if (!this.saveData[key]) {
                        this.errors.push('Value ' + data[key] + ' is not of type ' + this.schema[key]);
                    }
                }
            }
        }
    }

    return Model;
}();
