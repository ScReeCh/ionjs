const Components = require('../components/Components.js');

var router = {
    _routes: [],
    _routeIndex: {},
    loadDefault: function () {
        for (var i = 0, len = this._routes.length; i < len; i++) {
            var route = this._routes[i];
            if (typeof route.default != "undefined" && route.default == true) {
                this._routeIndex[route.view] = route;
                this.navigate(route.view);
            } else {
                this._routeIndex[route.view] = route;
            }
        }
    },
    setRoutes: function (routes) {
        this._routes = routes;
        this.loadDefault();
    },
    setRoute: function (route) {
        this._routeIndex[route.view] = route;
    },
    activeRoute: function (viewName, params) {
        var view = this._routeIndex[viewName];
        if (typeof view === "undefined") {
            console.warn('View component "' + viewName + '" does not exist.');
            return;
        }
        if (typeof view.authGuard != "undefined" && view.authGuard instanceof Object) {

            var authSevice = new view.authGuard();
            if (typeof authSevice.events === "undefined") {
                console.error('Your Authentication service is not extending "ionjs/core/Authentication" class.');
                return;
            }
            authSevice.events.on('authenticated', () => {
                view.instance = new Components(view.component, params, (parent) => {
                    this.setChildComponents(view.component.components, parent);
                });
            });
            // authSevice.then((response, err) => {
            //     if (err) {
            //         console.warn('Error in loading view!');
            //         return false;
            //     }
            //     if (response == null) {
            //         this.navigate("SignIn");
            //     } else {
            //
            //         view.instance = new Components(view.component, params, (parent) => {
            //             this.setChildComponents(view.component.components, parent);
            //         });
            //     }
            //
            // });
        } else {
            view.instance = new Components(view.component, params, (parent) => {
                this.setChildComponents(view.component.components, parent);
            });
        }
    },
    setChildComponents: function (components, parent) {
        for (var key in components) {
            if (typeof parent != "undefined") {
                components[key].parent = parent;
            }

            this.setRoute({
                view: components[key].view.name,
                component: components[key],
                activate: true
            });

            if (components[key].active) {
                this.activeRoute(components[key].view.name, {});
            }
        }
    },
    navigate: function (view, params) {
        if (typeof params == "undefined") {
            params = {};
        }

        this.activeRoute(view, params);
    },
    navigateView: function (view, params) {
        if (typeof params == "undefined") {
            params = {};
        }

        this.activeRoute(view, params);
    }
}

module.exports = router;
