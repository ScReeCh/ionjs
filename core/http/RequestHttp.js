'use strict';

const Request = require('request');

const defaultDomain = "http://192.168.0.17:3000/v1";

module.exports = function (options) {

    class RequestHttp {

        constructor(options) {
            return this.sendRequest(options);
        }

        setToken(token) {
            RequestHttp.token = token;
        }

        headers(token) {
            return {
                "Content-Type": "application/json",
                "Authorization": "bearer " + token
            };
        }

        sendRequest(options) {

            if (typeof options.url == "undefined") {
                options.url = defaultDomain;
            } else {
                options.url = defaultDomain + options.url;
            }

            if (typeof options.body == "undefined") {
                options.body = {
                    "empty": ""
                };
            }

            return new Promise((resolve, reject) => {
                var reg = Request({
                    method: options.method,
                    url: options.url,
                    json: options.body,
                    headers: this.headers(token)
                }, function (err, req, response) {

                    if (err) {
                        console.log(err);
                        reject(err);
                        return false;
                    }
                    console.log(req);
                    console.log(response);

                    response = response;
                    resolve(response);
                });
                console.log(reg);
            });
        }
    }

    return new RequestHttp(options);
};
