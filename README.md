The aim of Ionjs is to quickly get a [electron](http://electron.atom.io/) app up and running using the latest ES6 javascript specifications.

#Getting Started

 - Install
 - Folder structure
 - Routing
 - Authentication
 - Views
 - Controllers
 - Models (With NeDB JS DataBase integration)
 - Request
 - Services

#Install

npm install ionjs

**Demo application**
```
git clone https://github.com/HencoBurger/ionjs-demo.git
```
#Folder Structure

The folder structure is only a suggestion.  You can pretty much do what you want.
Also have a look at the [demo](https://github.com/HencoBurger/ionjs-demo) app for an example.

-| root

----| app

------------| controllers

------------ models

------------| services

------------| views

--------| app.js

--------| router.js

----| assets

----| db

----| src_assets

| main.js

#Routing
Don't think of routing in Ionjs as the routing you will find in a conventional web framework.  The router instance is more of a why to active view components within your electron application.

```javascript
const Router = require('ionjs/core/router');
```

**Example:**
```javascript
// ---- app/router.js

// Router instance
const Router = require('ionjs/core/router');

// Authentication service
const SignUp = require('./services/authentication/Authentication.js');

// View components
const SignUp = require('./controllers/signUp/SignUp.js');
const SignIn = require('./controllers/signIn/SignIn.js');
const MainView = require('./controllers/mainView/MainView.js');

Router.setRoutes([{
    view: "SignIn", // Name of view
    component: SignIn, // View class
    default: true // Default view to initiate on load
}, {
    view: "SignUp",
    component: SignUp
}, {
	view: "mainView",
	component: MainView,
	authGuard : auth_service
}]);
```

###Methods:

####Setup application main routing:

```javascript
Router.setRoutes( [{
	view : "view_name" ,
	component : view_class ,
	authGuard : auth_service ,
	default : true
}] );
```
**Optional:**

 - "default" - As the name would suggest, its
   the first view that gets activated when running the App.

 - "authGuard" - Used to secure a route view.

####Navigate to set view:
```javascript
Router.navigate("view_name", {param:"param_value"});
```

#Authentication
You can restrict access to the application on a per route view. (Refer to previous routing example).

### Auth Service
```javascript
// ---- app/services/authentication/Authentication.js

'use strict';

const Auth = require('ionjs/core/authentication');
const Router = require('ionjs/core/router');

module.exports = class Authentication extends Auth {
    authenticate(authenticated) {

        setTimeout(function () {
            Router.navigate("SignIn");
            //authenticated(true);
        }, 100);
    }
}

```

#Views

Iojs uses virtual DOM with Handlebars for template view creation.

Refer to [Handlebars API](http://handlebarsjs.com/).

####Event Handlers

To handle an event in the view you can add a event hook to any element in the view template.
To add a event you wrap the event key word in (brakets) and then assign that event to a function in your controller.

Example:
```javascript
<button type="button" (click)="changeString('Update View')">Update!</button>
```

**Events:**
```javascript
(blur)="blurFunction()"
(change)="changeFunction()"
(focus)="focusFunction()"
(input)="blurFunction()"
(keydown)="keydownFunction()"
(keypress)="keypressFunction()"
(keyup)="keyupFunction()"
(click)="clickFunction()"
(dblclick)="dblclickFunction()"
(wheel)="wheelFunction()"
(scroll)="scrollFunction()"
```
#Controllers
Controllers are view components, a view component can represent a single parent view or can be used as a component of a component.  Basically you can nest view components and share data between the parent component.

Example:
```javascript
// ---- app/controllers/signIn/SignIn.js

'use strict';

const Router = require('ionjs/core/router');
const User = require('../../models/user/User.js');

const SELECTOR = "main-window";
const TEMPLATE = "app/views/templates/signIn/sign-in";

var component = {
    "selector": SELECTOR,
    "template": TEMPLATE,
    "components": []
};

class SignIn {

    constructor() {
        this.user = {
            email_address: '',
            password: ''
        };
    }

    signIn() {
        User.signIn(this.user).then(function (response) {
            Router.navigate("MessageDashboard");
        });
    }

    navigate(view) {
        Router.navigate(view);
    }
}

component.view = SignIn;

module.exports = component;
```

#Models
Models are the connection between any external API data end-points or local storage.

Ionjs uses [NeDB](https://github.com/louischatriot/nedb) to manage its local 100% javascript database.
Reference [NeDB API](https://github.com/louischatriot/nedb).

Example:
```javascript
// ---- app/models/user/User.js

'use strict';

const Model = require('ionjs/core/model');

var db = {
    'location': __dirname + '/../../../db/user.db',
    'autoload': true,
    'schema': {
        'id': 'number',
        'first_name': 'string',
        'last_name': 'string',
        'email_address': 'string',
        'created_at': 'string',
        'created_at': 'string',
    }
};

module.exports = class User extends Model {
    constructor() {
        super(db);
    }

    insert(data, callback) {
        this.insert(data, function (err, newData) {
            if (err) {
                callback(err, null);
                return false;
            }
            callback(null, newData);
        });
    }

    getUser(callback) {
        this.findOne({}, function (err, docs) {
            if (err) {
                callback(err, null);
                return false;
            }
            callback(null, docs);
        });
    }
}
```

#Request
Ionjs extends the native Nodejs Router API.

Example:
```javascript
'use strict';

const RequestHttp = require('ionjs/core/http');
const Model = require('ionjs/core/model');

var db = {
    'location': __dirname + '/../../../db/user.db',
    'autoload': true,
    'schema': {
        'id': 'number',
        'first_name': 'string',
        'last_name': 'string',
        'email_address': 'string',
        'created_at': 'string',
        'created_at': 'string',
    }
};

module.exports = class User extends Model {
    constructor() {
        super(db);
    }

    getUser(callback) {
        this.findOne({}, function (err, docs) {
            if (err) {
                callback(err, null);
                return false;
            }

            RequestHttp({
                method: "GET",
                url: "/authuser",
            }).then((response, err) => {
                if (err) {
                    reject(err);
                    return false;
                }
                this.insert(response.data).then(() => {
                    RequestHttp.token = response.data.token;
                });
                getUserObservable(response.data[0]);
                resolve(response.data);
            });

            callback(null, docs);
        });
    }
}
```

#Services
